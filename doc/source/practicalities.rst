Practical matters
=================

Lecturers
---------

The module is led by `Dr Colin Cotter
<http://www.imperial.ac.uk/people/colin.cotter>`_ and `Dr David Ham
<http://www.imperial.ac.uk/people/david.ham>`_.

Accreditation
-------------

This module is an accredited elective in the `MSc in Applied
Mathematics
<http://www.imperial.ac.uk/study/pg/courses/mathematics/applied-mathematics/>`_
and the `MRes in Mathematics of Planet Earth <http://mpecdt.org>`_. It
is also an approved elective for second and third year PhD students in
the `CDT in Fluid Dymanics Accross Scales
<http://www3.imperial.ac.uk/fluidscdt>`_. Other masters and PhD
students wishing to take the module should contact the lecturers in
the first instance.

Assumed knowledge
-----------------

The theory component of the module will assume only a familiarity with
PDEs, vector calculus and basic undergraduate analysis. The
implementation part of the module additionally assumes some ability to
program in a high level language (for example Python, Matlab, Java, C
or C++). The implementation will be in Python, a very high level and
simple language with similarities to Matlab. Students who are not
familiar with Python will need to acquire some familiarity with the
language, for example by doing one of the suggested tutorials.


Assessment
----------

The theory part of the module will be assessed by an exam worth 50% of
the module. The implementation part of the module will be assessed by
submission of your working code implementing finite element in two
dimensions. This will also be worth 50%

Submitting code for feedback
............................

The assessment of your finite element implementation will occur in a
single submission at the end of term. However feedback on your
progress will be available during the term. Informal feedback will be
available during the labs, and you will have the opportunity to submit
your code for formal feedback every two weeks during the term. The
deadline for formal feedback submissions will be midnight on Sunday
nights and feedback will be provided before the next Friday lab.

Mastery
.......

All Department of Mathematics fourth year and masters courses must
contain a mastery component.

The mastery component of this module will be the extension exercise to
the implementation project. The mastery project will be handed out
approximately half way through term.


Course email list
-----------------

There is an email list for this module which will be used to
communicate during the term and to which you need to subscribe. Please
subscribe `here
<https://mailman.ic.ac.uk/mailman/listinfo/finite-element-course>`_. Failure
to subscribe to the email list will not be regarded as an excuse!
